package my.checksites;

import com.oracle.webservices.internal.api.databinding.DatabindingMode;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.nio.client.CloseableHttpAsyncClient;
import org.apache.http.impl.nio.client.HttpAsyncClientBuilder;
import org.apache.http.impl.nio.client.HttpAsyncClients;
import org.apache.http.nio.client.HttpAsyncClient;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.*;

public class RequestSender {

    RequestSender() {
        try {
            getUrlsFromFile();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    /**
     * Файл с данными
     * Одинаковые сайты лучше не записывать, а то словарям грустно будет
     */
    private File input = new File("input.txt");

    /**
     * Список сайтов из файла
     */
    private List<String> sites = new ArrayList<>();

    /**
     * Словарь для хранения общего времени выполнения запросов
     * Ключ - url сайта, значение - время выполнения
     */
    private Map<String, String> generalMap = new HashMap<>();

    /**
     * Словарь для хранения времени последовательного выполнения запросов
     */
    private Map<String, String> sequenceMap = new HashMap<>();

    /**
     * Словарь для хранения времени параллельного выполнения запросов
     */
    private Map<String, String> parallelMap = new HashMap<>();

    /**
     * Словарь для хранения времени асинхронного выполнения запросов
     */
    private Map<String, String> asyncMap = new HashMap<>();


    public void sendSeqRequests(){
        sites.forEach(site -> {
            HttpClient client = HttpClientBuilder.create().build();
            try {
                long t1 = System.currentTimeMillis();
                HttpResponse response = client.execute(new HttpGet(site));
                long t2 = System.currentTimeMillis();
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode>199 && statusCode<300)
                    sequenceMap.put(site, (t2-t1)+ " мс");
                else sequenceMap.put(site, "НЕДОСТУПЕН");

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendParallelRequests(){
        sites.parallelStream().forEach(site ->{
            HttpClient client = HttpClientBuilder.create().build();
            try {
                long t1 = System.currentTimeMillis();
                HttpResponse response = client.execute(new HttpGet(site));
                long t2 = System.currentTimeMillis();
                int statusCode = response.getStatusLine().getStatusCode();
                if (statusCode>199 && statusCode<300)
                    parallelMap.put(site, (t2-t1)+ " мс");
                else parallelMap.put(site, "НЕДОСТУПЕН");

            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }

    public void sendAsyncRequests(){
       // sites.forEach(s ->{
            //TODO if it's possible :(
       // });

    }

    public void printResultsToConsole(){
        
    }

    private void getUrlsFromFile() throws FileNotFoundException {
        Scanner sc = new Scanner(input);
        while (sc.hasNextLine()){
            sites.add(sc.nextLine());
        }

    }

    private void initMaps(List <String> a){
        sites.forEach(site -> {
            generalMap.put(site, null);
            sequenceMap.put(site, null);
            parallelMap.put(site, null);
            asyncMap.put(site, null);
        });
    }
    
    private void recalculateGeneralMap(){
        sites.forEach(site -> {
           /* Long time = sequenceMap.get(site);
            generalMap.put(site, )
            */
        });
    }
}
